﻿using System;
using System.Web.Script.Serialization;
using FlowQuestServerActivity;
using System.Net.Http;

namespace EvaluationLib
{   
    public class EvaluationLib
    {
        public class ReturnResult
        {
            public bool isError { get; set; }
            public string result { get; set; }
            public string errorMessage { get; set; }
        }


        [ServerActivity("Set Reviewer As Approval")]
        public class SetReviewerAsApproval : IServerActivity
        {
            public void ExecuteServerCode(IHostContext hostContext)
            {
                var serviceUrl = hostContext.ProcessVariables["serviceUrl"].Value;
                //var serviceUrl = "http://tesapp1234.azurewebsites.net/Service1.svc/";
                var client = new HttpClient();
                var uri = new Uri(serviceUrl + "GetIdReviewer/" + hostContext.ProcessInfo.ProcessId);
                //var uri = new Uri(serviceUrl + "GetIdReviewer/" + "18475");
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                var serializer1 = new JavaScriptSerializer();
                var objResult = serializer1.Deserialize<ReturnResult>(result);
                if (objResult.isError)
                {
                    hostContext.ProcessVariables.SetValue("errorMessage", objResult.errorMessage);
                    throw new Exception(objResult.errorMessage);
                }
                else
                {
                    hostContext.ProcessVariables.SetValue("reviewer", objResult.result);                 
                }
            }
        }

        [ServerActivity("Set Manager As Approval")]
        public class SetManagerAsApproval : IServerActivity
        {
            public void ExecuteServerCode(IHostContext hostContext)
            {
                var serviceUrl = hostContext.ProcessVariables["serviceUrl"].Value;

                var client = new HttpClient();
                var uri = new Uri(serviceUrl + "GetIdManager/" + hostContext.ProcessInfo.ProcessId);
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                var serializer1 = new JavaScriptSerializer();
                var objResult = serializer1.Deserialize<ReturnResult>(result);
                if (objResult.isError)
                {
                    hostContext.ProcessVariables.SetValue("errorMessage", objResult.errorMessage);
                    throw new Exception(objResult.errorMessage);
                }
                else
                {
                    hostContext.ProcessVariables.SetValue("manager", objResult.result);
                }
            }
        }

        [ServerActivity("Set Senior Manager As Approval")]
        public class SetSeniorManagerAsApproval : IServerActivity
        {
            public void ExecuteServerCode(IHostContext hostContext)
            {
                var serviceUrl = hostContext.ProcessVariables["serviceUrl"].Value;

                var client = new HttpClient();
                var uri = new Uri(serviceUrl + "GetIdSeniorManager/" + hostContext.ProcessInfo.ProcessId);
                var response = client.GetAsync(uri).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                var serializer1 = new JavaScriptSerializer();
                var objResult = serializer1.Deserialize<ReturnResult>(result);
                if (objResult.isError)
                {
                    hostContext.ProcessVariables.SetValue("errorMessage", objResult.errorMessage);
                    throw new Exception(objResult.errorMessage);
                }
                else
                {
                    hostContext.ProcessVariables.SetValue("managerofmanager", objResult.result);
                }
            }
        }


    }
}
